angular.module('listasApp', [])
.controller('ListasController', function( $scope ) {

	$scope.mostrarOpciones = false;

	$scope.listaCategorias = [];

	$scope.nuevaCategoria = {
		nombre: "",
		parent: 0
	}

	$scope.fnGuardar = function(){

		if ( !$scope.nuevaCategoria.id ) {
			var visible = true;
			var nivel = 1;
			if ( $scope.nuevaCategoria.parent > 0 ) {
				visible = false;
				nivel = $scope.getCategoria( $scope.nuevaCategoria.parent ).nivel + 1;
			}


			var nuevaCategoriaTmp = {
				id: $scope.listaCategorias.length + 1,
				nombre: $scope.nuevaCategoria.nombre,
				collapse: true,
				parent: $scope.nuevaCategoria.parent,
				selected: false,
				totalSubcategorias: $scope.listSubCategorias( $scope.nuevaCategoria ).length,
				visible: visible,
				nivel: nivel,
				alias: ""
			}

			nuevaCategoriaTmp.alias = $scope.fnGenerarAlias( nuevaCategoriaTmp, "" )

			$scope.listaCategorias.push( nuevaCategoriaTmp );

			$scope.nuevaCategoria = {
				nombre: "",
				parent: 0
			}

			return;
		}

		$scope.getCategoria( $scope.nuevaCategoria.id ).nombre = $scope.nuevaCategoria.nombre;

		$scope.nuevaCategoria = {
			nombre: "",
			parent: 0
		}
	}
	

	$scope.fnSeleccionar = function( categoria ){
		categoria.selected = !categoria.selected;

		if (categoria.selected) {
			$scope.mostrarOpciones = true;
		}else{
			$scope.mostrarOpciones = false;
		}
	}

	$scope.fnSeleccionarNivel = function( categoria ) {
		if ( $scope.listSubCategorias( categoria ).length === 0 ) {

			angular.forEach( $scope.listaCategorias.filter( item => item.selected === true ), function(item) {
				if (item.id !== categoria.id) {
					item.selected = false;
				}
			})
			categoria.selected = !categoria.selected;

			if (categoria.selected) {
				$scope.mostrarOpciones = true;
			}else{
				$scope.mostrarOpciones = false;
			}

		}else{
			$scope.fnValidarNiveles( categoria, $scope.listaCategorias )
		}
	}
	$scope.fnValidarNiveles = function( categoria, listaCategorias ){
		if ( categoria.collapse ) {
			categoria.collapse = false;
			angular.forEach( $scope.listSubCategorias( categoria ), function( item ){
				item.visible = true;
			} );
		}else{
			categoria.collapse = true;
			angular.forEach( $scope.listSubCategorias( categoria ), function( item ){
				$scope.fnCerrarNiveles(item, listaCategorias);
			} );
		}
	}
	$scope.fnCerrarNiveles = function( categoria, listaCategorias ) {
		categoria.visible = false;
		categoria.collapse = false;
		angular.forEach( $scope.listSubCategorias( categoria ), function( item ){
			item.visible = false;
			$scope.fnCerrarNiveles(item, listaCategorias);
		} );
	}

	$scope.listaOrdenada = function() {
		var nuevaLista = [];
		angular.forEach( $scope.listSubCategorias( {id: 0} ) , function( item ){
			$scope.agregarNiveles( item, nuevaLista, $scope.listSubCategorias(item) )
		} )
		return nuevaLista;
	}

	$scope.agregarNiveles = function( categoria, lista, hijos ) {
		lista.push( categoria )
		angular.forEach( hijos, function(item){
			if ( $scope.listSubCategorias(item).length > 0 ) {
				$scope.agregarNiveles( item, lista, $scope.listSubCategorias(item))
			}else{
				lista.push( item )
			}
		} )
	}

	$scope.getIndex = function ( id ){
		var idx = 0
		angular.forEach( $scope.listaCategorias, function( item, index ){
			if (item.id === id) {
				idx = index;
			}
		} );
		return idx;
	}
	$scope.getCategoria = function ( id ) {
		return $scope.listaCategorias.find(item => item.id === id)
	}
	$scope.listSubCategorias = function( categoria ) {
		return $scope.listaCategorias.filter(item => item.parent === categoria.id);
	}

	$scope.fnGenerarAlias = function( categoria, nombre ){
		if (categoria.nivel > 1) {
            var padre = $scope.getCategoria(categoria.parent)
            if (nombre != "") {
                return $scope.fnGenerarAlias(padre, categoria.nombre + " > " + nombre) ;
            } else {
                return $scope.fnGenerarAlias(padre, categoria.nombre) ;
            }
        } else {
            if (nombre != "") {
                return categoria.nombre + " > " +  nombre;
            } else {
                return categoria.nombre;
            }
        }
	}

	$scope.fnAccion = function( accion ){
		var categoriaSeleccionada = $scope.listaCategorias.find( item => item.selected === true )
		if (accion === 1) {
			$scope.nuevaCategoria = categoriaSeleccionada;
		}else if (accion === 2) {
			if ( $scope.listSubCategorias( categoriaSeleccionada ).length > 0 ) {
				$scope.borrarHijos( categoriaSeleccionada )
			}else{
				$scope.listaCategorias.splice(  $scope.listaCategorias.indexOf( categoriaSeleccionada ) , 1 )
			}
		}
	}

	$scope.borrarHijos = function( categoria ){
		angular.forEach( $scope.listSubCategorias(categoria) , function(item){
			$scope.borrarHijos(item);
		})
		$scope.listaCategorias.splice(  $scope.listaCategorias.indexOf( categoria ) , 1 )
	}

})
